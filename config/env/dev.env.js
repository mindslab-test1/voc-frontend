module.exports = {
    NODE_ENV: 'development',
    FAKE_DATA: true,
    API_BASE_URL: '/api',
};
