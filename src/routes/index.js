import Vue from 'vue';
import Router from 'vue-router';
import store from '../store/index';
import { RepositoryFactory } from '@/repositories/RepositoryFactory';

const UserRepository = RepositoryFactory.get('user');

Vue.use(Router);

const alreadyLoggedRouting = async (to, from, next) => {
    const userString = localStorage.getItem('access_token');
    if (userString) {
        try {
            let { data } = await UserRepository.me();
            let url = '';
            switch (data.userRole) {
                /*case "ROLE_ADMIN":
                case "ROLE_QA_ADMIN":
                case "ROLE_UW_ADMIN": url = '/main/dashboard'; break;
                case "ROLE_QA_REVIEWER":
                case "ROLE_UW_REVIEWER": url = '/qa/assign/reviewer'; break;
                case "ROLE_TM_CENTER": url = '/qa/result'; break;*/
                default: url = '/main/dashboard';
            }

            return next(url);
        } catch (err) {
            console.log(err)
        }
    }

    next();
};

const checkForUserAuth = async (to, from, next) => {
    let loggedIn = false;
    try {
        let { data } = await UserRepository.me();
        store.commit('user/setUserInfo', data);
        loggedIn = true;
    } catch (err) {
        console.log(err)
    }

    if (loggedIn === false) {
        store.commit('user/clearUserInfo')
        /* alert('로그인이 필요한 기능입니다.') */
        return next({
            name: 'Login',
            params: {
                next: to.fullPath
            },
        });
    }

    /*const {data: pageAuthCheck} = await UserRepository.pageAuthCheck(to.path);
    if(!pageAuthCheck){
        alert('페이지 접근 권한이 없습니다.');
        return next({
            name: 'Login'
        });
    }*/

    const key = new Date().getMilliseconds();
    if (store.getters["base/keepComponents"].length > 0) {
        if (!to.query.key) {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: `${to.fullPath}?key=${key}`, korName: to.meta.korName });
            to.params.key = `${to.name}-${new Date().getMilliseconds()}`;
        } else {

        }
    } else {
        if (to.query.key) {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: to.fullPath, korName: to.meta.korName });
        } else {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: `${to.fullPath}?key=${key}`, korName: to.meta.korName });
            to.params.key = `${to.name}-${key}`;
        }
    }
    next()
};

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'Login',
        beforeEnter: alreadyLoggedRouting,
        component: require('@/components/login/Login').default,
        meta: {
            title: 'VOC - LOGIN',
            layout: 'LoginLayout',
        }
    },
    {
        path: '/main/dashboard',
        name: 'Dashboard',
        beforeEnter: checkForUserAuth,
        component: require('@/components/main/Dashboard').default,
        meta: {
            title: 'TMQA - MAIN - DASHBOARD',
            location: 'MAIN - DASHBOARD',
            menu: 'HOME',
            korName: 'Dashboard'
        }
    },
	{
		path: '*',
		name: 'notFound',
		redirect: '/'
	},
];
const router = new Router({
    mode: 'history',
    routes: routes
});

const DEFAULT_TITLE = 'TMQA';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;