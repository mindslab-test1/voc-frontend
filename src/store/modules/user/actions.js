import 'url-search-params-polyfill';
import { RepositoryFactory } from '@/repositories/RepositoryFactory';

const userRepository = RepositoryFactory.get('user');

const signIn =  ({ commit }, loginObj) => { 
    let params = {
        'userId': loginObj.userId,
        'userPw': loginObj.userPw,
    };
    return new Promise((resolve, reject) => {
        userRepository.sign_in(params).then((res) => {
            const {accessToken, userInfo} = res.data;
            commit('setUserToken', accessToken);
            commit('setUserInfo', userInfo);
            return resolve();
        }).catch((error) => {
            return reject({rMsg : `로그인 실패("${error.response.data.message}")`});
        })
    });
};

/* const signOut =  ({ commit, state, getters }) => {

    let userNo = state.userInfo.userNo;
    let refresh_token = getters.getRefreshTokenFromLocalStorage();
    
    return new Promise((resolve, reject) => {
        if(!refresh_token) {
            commit('clearUserInfo')
            return resolve("로그아웃 처리 되었습니다.");
        }else {
            let params = { 'userNo': userNo, 'refresh_token' : refresh_token };
            userRepository.sign_out(params).then((res) => {
                commit('clearUserInfo')
                return resolve(res.data.processMessage);
            }).catch((error) => {
                return reject("로그아웃 실패(" + error.response.status + ")");
            });
        }
    });
}; */

const signOut =  ({ commit, state, getters }) => {
    return new Promise((resolve, reject) => {
        commit('clearUserInfo');
        return resolve('로그아웃 처리 되었습니다.');
        /*userRepository.sign_out().then((res) => {
            commit('clearUserInfo');
            return resolve(res.data.processMessage);
        }).catch((error) => {
            return reject("로그아웃 실패(" + error.response.status + ")");
        });*/
    });
}

const signUp =  ({ commit }, signUpObj) => {
    return new Promise((resolve, reject) => {
        userRepository.sign_up(signUpObj).then((res) => {
            const data = res.data;
            if(data.processStatus == "SIGNUP_DUPLICATE") {
                return reject(data.processMessage)
            }
            commit('clearUserInfo')
            return resolve(data.processMessage);
        }).catch((error) => {
            return reject("회원가입 실패(" + error.response.status + ")");
        });
    });
};

export default {
    signIn,
    signOut,
    signUp,
};