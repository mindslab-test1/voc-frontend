const getRefreshTokenFromLocalStorage = () => ()  =>{
    return localStorage.getItem("refresh_token") || '';
};

const getUserInfo = (state) => state.userInfo || {};

export default {
    getRefreshTokenFromLocalStorage,
    getUserInfo,
};