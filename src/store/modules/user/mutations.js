const setUserInfo = (state, userInfo) => {
    state.userInfo = userInfo || {}
};

const setUserToken = (state, access_token) => {
    if(access_token) {
        localStorage.setItem("access_token", access_token);
    }
    /*if(refresh_token) {
        localStorage.setItem("refresh_token", refresh_token);
    }*/
};

const clearUserInfo = (state) => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("user");
    state.userInfo = {};
};

export default {
    setUserInfo,
    setUserToken,
    clearUserInfo,
};