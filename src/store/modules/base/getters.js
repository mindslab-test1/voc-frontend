
const isModal = (state) => state.modalStates.length > 0;

const keepComponents = (state) => state.keepComponents;

export default {
    isModal,
    keepComponents,
};