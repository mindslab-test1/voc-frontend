const clearModalStates = (state) => {
    state.modalStates = [];
};
const setModalStates = (state, targetKeys) => {
    state.modalStates = targetKeys || [];
};

const removeModalStates = (state, removeKeys) => {
    let modalStates = state.modalStates;
    removeKeys.forEach(targetKey => {
        let indexs = modalStates.map((modalState, idx) => modalState.name == targetKey ? idx : '').filter(String);
        for(let index in indexs) {
            modalStates.splice(index, 1)
        }
    });
    state.modalStates = modalStates;
};

const setComponent = (state, component) => {
    let components = state.keepComponents;
    const index = components.findIndex(v => v.routeName === component.routeName);
    if(index > -1){
        components.splice(index, 1);
    }

	if(state.keepComponents.length >= 10) {
		components.shift();
	}
	components.push(component);
    state.keepComponents = components;
};

const clearComponent = (state, component) => {
    state.keepComponents = [];
    component.router.push('/');
};

const removeComponentByKey = (state, component) => {
    if(state.keepComponents.length > 1){
        const equal = component.route.name === component.routeName
        const index = state.keepComponents.findIndex(v => v.routeName === component.routeName);
        if(index > -1){
            if(equal){
                component.router.push(state.keepComponents[index === 0 ? index+1 : index-1].to);
            }
            state.keepComponents = state.keepComponents.filter(v => v.routeName !== component.routeName);
        }
    }
};

export default {
    clearModalStates,
    setModalStates,
    removeModalStates,
    setComponent,
    clearComponent,
    removeComponentByKey,
};