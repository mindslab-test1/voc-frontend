import "chartjs-chart-box-and-violin-plot";
import { Bar, generateChart, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;

export default {
    extends: Bar,
    mixins: [reactiveProp],
    props: ["options"],
    mounted() {
        this.renderChart(this.chartData, this.options);
    }
};