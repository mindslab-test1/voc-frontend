import Repository from "./Repository";

const resource = "/dashboard";

export default {
    getPieChartData(data) {
        return Repository.post(`${resource}/getPieChartData`, data);
    },
    getCenterListData(data) {
        return Repository.post(`${resource}/getCenterList`, data);
    },
    getCenterTaChartData(data) {
        return Repository.post(`${resource}/getPerCenterTaChart`, data);
    },
    getCenterTmrChartData(data) {
        return Repository.post(`${resource}/getPerCenterTmrChart`, data);
    },
    getMonthTaScoreChartData(data) {
        return Repository.post(`${resource}/getMonthTaScoreChart`, data);
    },
};