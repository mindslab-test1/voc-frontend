import Repository from "./Repository";

export default {
    getCodeDetailByGroupCode(codeGroup, sort, cdNm, cd) {
        return Repository.get(`codes/${codeGroup}?`+(sort != undefined && sort != null && sort instanceof Object ? Object.keys(sort).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(sort[key])).join('&') : '')+(!cdNm ? '' : '&cdNm='+cdNm)+(!cd ? '' : '&cd='+cd));
    },
    setCodeDetail(param) {
        return Repository.post(`code`, param);
    },
    updCodes(param) {
        return Repository.patch(`code`, param);
    },
    delCodes(data) {
        return Repository.delete(`code`, data);
    },
};