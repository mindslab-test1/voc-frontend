import axios from "axios";
import TokenFactory from './TokenFactory';
import router from '@/routes';
import store from '@/store';

const baseURL = process.env.API_BASE_URL;
const instance = axios.create({
  baseURL
});

instance.defaults.timeout = 30000;

instance.interceptors.request.use((config) => {

  let access_token = localStorage.getItem('access_token') || '';
  if(access_token) {
    config.headers.Authorization = `Bearer ${access_token}`;
  }

  return config
}, (error) => {
  return Promise.reject(error)
})

instance.interceptors.response.use( (response) => {
  return response;
}, (error) => {
  let originalRequest = error.config;
  if (error.response.status === 401 && !originalRequest._retry) {
    
    if(error.config.url == '/user/sign-in' || error.config.url == '/user/sign-out') {
      return Promise.resolve(error.response)
    }

    /* if(error.config.url =='/user/me') {
      return Promise.reject(error.response)
    } */

    if (error.config.url == '/user/auth/refresh_token' || error.response.errorEngMessage == 'DisabledException: User is disabled') {
      store.commit('user/clearUserInfo');
      alert('로그인이 필요합니다.');
      store.commit('base/clearModalStates');
      router.push({ name: 'Login' });
      return Promise.reject(error)
    }
    
    /*originalRequest._retry = true;
    return TokenFactory().then(({processStatus, processMessage, access_token, refresh_token}) => {
      //console.log("token 처리 결과 : " + processStatus)
      if(!processStatus) {
        console.log("processMessage : ", processMessage)
        store.commit('user/clearUserInfo');
        alert('로그인이 필요합니다.');
        store.commit('base/clearModalStates');
        router.push({ name: 'Login' });
        return Promise.reject(error)
      }

      let updateUserToken = { 'access_token' : access_token };
      //console.log("access_token : " + access_token);
      if(refresh_token) {
        updateUserToken['refresh_token'] = refresh_token;
        console.log("refresh_token : " + refresh_token)
      }
      store.commit('user/setUserToken', updateUserToken);
      
      // request with new token
      const config = error.config;
      config.headers['Authorization'] = `Bearer ${access_token}`;

      return new Promise((resolve, reject) => {
        axios.request(config).then(response => {
          console.log("reTry resolve : url(%s) : %d", response.config.url, response.status)
          resolve(response);
        }).catch((error) => {
          console.log("reTry reject : " + error)
          reject(error);
        })
      });
    })*/
  }else if(error.response.status === 403) {
    store.commit('base/setModalStates', [{name: 'ForbiddenModal', parameters: {}}]);
  }else if(error.response.data.exception === 'TokenDecodeException'){
    store.commit('user/clearUserInfo');
  }
  alert(error.response.data.message);
  return Promise.reject(error);
  
});

export default instance; 