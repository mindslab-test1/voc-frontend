import axios from 'axios'
const instance = axios.create({ 'baseURL' : process.env.API_BASE_URL });
instance.defaults.timeout = 3000;

const REFRESH_TOKEN_KEY = 'refresh_token';
const getRefreshToken = () => {
    return localStorage.getItem(REFRESH_TOKEN_KEY);
};

const getNewAccessTokenFromRefreshToken = () => {
    return new Promise((resolve, reject) => {
        let refresh_token = getRefreshToken() || '';
        if(refresh_token) {
            instance.post('/user/auth/refresh-token', { refresh_token }).then(response =>{
                const {processStatus, processMessage, access_token, refresh_token } = response.data;
                resolve({ processStatus, processMessage, access_token, refresh_token });
            }).catch((error) => {
                resolve({'processStatus' : 'NEW_TOKEN_ISSUE_FAIL', 'processMessage': 'system error(' + error.message + ')' });
            });
        } else {
            resolve({'processStatus' : 'REFRESH_TOKEN_NOT_FOUND', 'processMessage': 'token does not exist'});
        }
    });
};
export default getNewAccessTokenFromRefreshToken;