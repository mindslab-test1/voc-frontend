import Repository from "./Repository";

const resource = "/user";
export default {
    sign_in(authObj) {
        return Repository.post(`${resource}/sign-in`, authObj)
    },
    sign_out() {
        return Repository.post(`${resource}/sign-out`)
    },
    /* sign_up(paramObj) {
        return Repository.post(`${resource}/sign_up`, paramObj)
    },
    profanityCheck() {
        return Repository.get(`${resource}/profanity`)
    },
    setProfanityAgree() {
        return Repository.post(`${resource}/profanity`)
    }, */
    me() {
        return Repository.get(`${resource}/me`)
    },
    pageAuthCheck(path) {
        return Repository.get(`${resource}/page/auth/check?path=${path}`)
    },
    /* changePassword(currentUserPw, newUserPw) {
        return Repository.put(`${resource}/me`, {currentUserPw: currentUserPw, userPw: newUserPw})
    } */
}