import UserRepository from './userRepository';
import CodeRepository from './codeRepository';
import HomeRepository from './homeRepository';


const repositories = {
  code: CodeRepository,
  user: UserRepository,
  home: HomeRepository,
  // other repositories ...
};

export const RepositoryFactory = {
  get: (name) => repositories[name],
};
