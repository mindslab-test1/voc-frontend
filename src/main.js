import Vue from 'vue';

import App from './App';
import router from './routes';
import store from './store';
import _ from 'lodash';
import filter from "@/utils/filters";
import vuetify from '@/plugins/vuetify' // path to vuetify export
import moment from 'moment'
import {ko} from 'vuejs-datepicker/dist/locale'
import codeRepository from "@/repositories/codeRepository";
import keepAlive from "@/utils/keepAlive";
import Vuelidate from 'vuelidate'

Vue.config.productionTip = false;

Vue.use(Vuelidate);

Vue.component('BaseKeepAlive', keepAlive);

// GLOBAL MIXIN
Vue.mixin({
    data() {
        return {
            moment: moment,
            ko: ko,
        }
    },
    methods: {
        /**
         * 공통코드 목록 조회
         * @param groupCode
         * @param sort
         * @returns {Promise<*>}
         */
        async getCodeDetailByGroupCode(groupCode, sort, cdNm, cd){
            const {data: result} = await codeRepository.getCodeDetailByGroupCode(groupCode, sort, cdNm, cd);
            if(result.status === 'NOT_FOUND') console.log('조회결과 없음 - 공통코드 목록');
            return result.data;
        },
    },
});

new Vue({
    filter,
    vuetify,
    moment,
    router,
    _,
    store,
    el: '#app',
    render: h => h(App),
});