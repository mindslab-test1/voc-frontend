/**
 * 팝업 제어를 위한 vm set
 * @param component
 */
const setVm = (component) => {
    window.vm = component;
}

/**
 * 팝업 생성
 * @param component
 * @param size
 * @param param
 */
const openPopup = (component, routeName, size, param, name) => {
    const path = component.$router.resolve({name: routeName, query: param}).href;
    window.open(path, name, 'width='+size.width+', height='+size.height);
}

/**
 * 팝업 부모 함수 호출
 * @param window
 * @param functionName
 * @param param
 */
const callOpenerFunction = (window, functionName, param) => {
    if(typeof window.opener.vm[functionName] === 'function'){
        window.opener.vm[functionName](param);
    }else{
        if(window.webpackHotUpdate){
            alert('parent component function not found');
        }
    }
}

export default {
    setVm,
    openPopup,
    callOpenerFunction
}