function saveAuthToCookie(value) {
    document.cookie = `tts_auth=${value}`;
}

function saveUserToCookie(value) {
    document.cookie = `tts_user=${value}`;
}

function getAuthFromCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)tts_auth\s*=\s*([^;]*).*$)|^.*$/, '$1');
}
  
function getUserFromCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)tts_user\s*=\s*([^;]*).*$)|^.*$/, '$1');
}
  
function deleteCookie(value) {
    document.cookie = `${value}=; expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
}

function setCookie(cookieName, cookieValue, cookieExpire, cookiePath, cookieDomain, cookieSecure){
    var cookieText=escape(cookieName)+'='+escape(cookieValue);
    cookieText+=(cookieExpire ? '; EXPIRES='+cookieExpire.toGMTString() : '');
    cookieText+=(cookiePath ? '; PATH='+cookiePath : '');
    cookieText+=(cookieDomain ? '; DOMAIN='+cookieDomain : '');
    cookieText+=(cookieSecure ? '; SECURE' : '');
    document.cookie=cookieText;
}
 
function getCookie(cookieName){
    var cookieValue=null;
    if(document.cookie){
        var array=document.cookie.split((escape(cookieName)+'='));
        if(array.length >= 2){
            var arraySub=array[1].split(';');
            cookieValue=unescape(arraySub[0]);
        }
    }
    return cookieValue;
}

export default { 
    saveAuthToCookie, 
    saveUserToCookie, 
    getAuthFromCookie, 
    getUserFromCookie, 
    deleteCookie,
    setCookie,
    getCookie,
};