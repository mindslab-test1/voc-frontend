const formatDate = (value) => {
    const date = new Date(value);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = month > 9 ? month : `0${month}`;
    const day = date.getDate();
    return `${year}-${month}-${day}`;
};

const numberPad = (n) => {
    var width = 3;
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
};
const comma = (val) => {
    return String(val).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

import Vue from 'vue';
Vue.filter('formatDate', formatDate)
Vue.filter('numberPad', numberPad)
Vue.filter('comma', comma)

export default {
    formatDate,
    numberPad,
    comma
}